<?php
/**
 * yibra functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package yibra
 */

if (!function_exists('yibra_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function yibra_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on yibra, use a find and replace
         * to change 'yibra' to the name of your theme in all the template files.
         */
        load_theme_textdomain('yibra', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'menu-1' => esc_html__('Primary', 'yibra'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('yibra_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', array(
            'height' => 250,
            'width' => 250,
            'flex-width' => true,
            'flex-height' => true,
        ));
    }
endif;
add_action('after_setup_theme', 'yibra_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function yibra_content_width()
{
    $GLOBALS['content_width'] = apply_filters('yibra_content_width', 640);
}

add_action('after_setup_theme', 'yibra_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function yibra_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'yibra'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'yibra'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'yibra_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function yibra_scripts()
{
    wp_enqueue_style('yibra-style', get_stylesheet_uri());
    wp_enqueue_script('yibra-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true);
    wp_enqueue_script('yibra-src', get_template_directory_uri() . '/js/src.js', array('jquery'), '20151215', true);
    wp_enqueue_script('yibra-slick', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js', array('jquery'), '20151215', true);
    wp_enqueue_script('yibra-materializejs', 'https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/js/materialize.min.js', array(), '20151215', true);
    wp_localize_script('yibra-src', 'ajaxurl', admin_url('admin-ajax.php'));
    wp_enqueue_style('yibra-materialize', 'https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/css/materialize.min.css');

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'yibra_scripts');

function rudr_instagram_api_curl_connect( $api_url ){
    $connection_c = curl_init(); // initializing
    curl_setopt( $connection_c, CURLOPT_URL, $api_url ); // API URL to connect
    curl_setopt( $connection_c, CURLOPT_RETURNTRANSFER, 1 ); // return the result, do not print
    curl_setopt( $connection_c, CURLOPT_TIMEOUT, 20 );
    $json_return = curl_exec( $connection_c ); // connect and get json data
    curl_close( $connection_c ); // close connection
    return json_decode( $json_return ); // decode and return
}





/**
 * Api connect Insgram
 */
function connectApi($atts)
{
    $access_token = '6229461036.da06fb6.4c7ffacce5174c899bf49041fd2a0103';
    $username = 'atelieryibra';
    $user_search = rudr_instagram_api_curl_connect("https://api.instagram.com/v1/users/search?q=" . $username . "&access_token=" . $access_token);
// $user_search is an array of objects of all found users
// we need only the object of the most relevant user - $user_search->data[0]
// $user_search->data[0]->id - User ID
// $user_search->data[0]->first_name - User First name
// $user_search->data[0]->last_name - User Last name
// $user_search->data[0]->profile_picture - User Profile Picture URL
// $user_search->data[0]->username - Username

    $user_id = $user_search->data[0]->id; // or use string 'self' to get your own media
    $return = rudr_instagram_api_curl_connect("https://api.instagram.com/v1/users/" . $user_id . "/media/recent?access_token=" . $access_token);

//var_dump( $return);


    echo '<div class="feeds_instgram_yp_home" id="feeds_instgram_yp_home">';
    foreach ($return->data as $feed) {
        $text = "";
        if (isset($feed->caption->text)) {
        $text = $feed->caption->text ;
        }  else {
        $text = '';
        }
        ?>

        <div class="feeds_instgram_yp_item_home " >
            <div class="feeds_instgram_yp_images"
                 style="background-image: url('<?php echo $feed->images->low_resolution->url; ?>') ">


            </div>
            <a class="feeds_instgram_yp_text" href="<?php echo $feed->link; ?>" target="_blank">

                <div class="txt_instgram"><span class="icon_instgram"><i class="fa fa-instagram" aria-hidden="true"></i></span> <?php echo $text; ?>
                </div>
            </a>
        </div>

        <?php
        $i=0;
        $i++;

        if ($i >= 10) {
            break;
        }
    }
    echo '</div>';
}


add_shortcode('connectApi', 'connectApi');

add_action('wp_ajax_mon_action', 'connectApiAll');
add_action('wp_ajax_nopriv_mon_action', 'connectApiAll');


function connectApiAll()
{

    $param = $_POST['url_instagram'];
    $url =  'https://igpi.ga/atelieryibra/media/?count=12';

    if (isset($_POST['url_instagram'])) {
        $url = $_POST['url_instagram'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        curl_close($ch);

        $feeds = json_decode($result);
        foreach ($feeds->items as $feed) { ?>

            <div class=" col s12 m4 3 feeds_">
                <div class="feeds_instgram_yp_images"
                     style="background-image: url('<?php echo $feed->images->low_resolution->url; ?>') ">


                </div>
                <a class="feeds_instgram_yp_text" href="<?php echo $feed->link; ?>" target="_blank">

                    <div class="txt_instgram"><span class="icon_instgram"><i class="fa fa-instagram"
                                                                             aria-hidden="true"></i></span> <?php echo $feed->caption->text; ?>
                    </div>
                </a>
            </div>

            <?php

        }

        echo '<input type="hidden" class="next_instagram" value="' . $feeds->next . '">';

        echo "<div class='container_load_more_instagram row'> <a class='load_more_instagram col s12 m12' href='#'><div class='loading_content'></div><span>+</span></a> </div>";
        die();
    } else {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        curl_close($ch);

        $feeds = json_decode($result);
        echo '<div class="feeds_instgram_yp_all2 row ">';
        foreach ($feeds->items as $feed) { ?>

            <div class=" col s12 m3 feeds_ ">
                <div class="feeds_instgram_yp_images"
                     style="background-image: url('<?php echo $feed->images->low_resolution->url; ?>') ">


                </div>
                <a class="feeds_instgram_yp_text" href="<?php echo $feed->link; ?>" target="_blank">

                    <div class="txt_instgram"><span class="icon_instgram">
                            <i class="fa fa-instagram" aria-hidden="true"></i></span> <?php echo $feed->caption->text; ?>
                    </div>
                </a>
            </div>

            <?php

        }

        echo '<input type="hidden" class="next_instagram" value="' . $feeds->next . '">';

        echo "</div>";


        echo "<div class='container_load_more_instagram row'> <a class='load_more_instagram col s12 m12' href='#'><div class='loading_content'></div><span>+</span></a> </div>";
    }



}


add_shortcode('connectApiAll', 'connectApiAll');


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
    require get_template_directory() . '/inc/jetpack.php';
}

add_action('admin_init', 'register_mysettings');
add_action('admin_menu', 'add_global_custom_options');

add_action('init', 'create_post_type');
function create_post_type()
{
    register_post_type('slider',
        array(
            'labels' => array(
                'name' => __('slider Home'),
                'singular_name' => __('slider')
            ),
            'public' => true,

        )
    );
}

function register_mysettings()
{
    register_setting('myoption-group', 'facebook_link');
    register_setting('myoption-group', 'youtube_link');
    register_setting('myoption-group', 'instagram_link');
    register_setting('myoption-group', 'twitter_link');
}

function add_global_custom_options()
{

    add_options_page('Social Network', 'Social Network', 'manage_options', 'social-network', 'global_custom_options');
}

// create function to display link socila network

function global_custom_options()
{ ?>
    <div class="wrap">
        <h2>One More parametres global</h2>

        <form method="post" action="options.php">
            <?php settings_fields('myoption-group'); ?>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Page Facebook :</th>
                    <td><input style="width:80%" type="text" name="facebook_link"
                               value="<?php echo get_option('facebook_link'); ?>"/></td>
                </tr>

                <tr valign="top">
                    <th scope="row">Page Youtube :</th>
                    <td><input style="width:80%" type="text" name="youtube_link"
                               value="<?php echo get_option('youtube_link'); ?>"/></td>
                </tr>

                <tr valign="top">
                    <th scope="row">Page Instagram:</th>
                    <td><input style="width:80%" type="text" name="instagram_link"
                               value="<?php echo get_option('instagram_link'); ?>"/></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Page Twitter:</th>
                    <td><input style="width:80%" type="text" name="twitter_link"
                               value="<?php echo get_option('twitter_link'); ?>"/></td>
                </tr>
            </table>

            <p class="submit">
                <input type="submit" class="button-primary" value="<?php _e('Enregistrer') ?>"/>
            </p>

        </form>
    </div>
    <?php


}

function my_custom_post_types()
{
    $labels = array(
        'name' => 'Médiathèque',
        'singular_name' => 'Médiathèque',
        'menu_name' => 'Médiathèque',
        'name_admin_bar' => 'Médiathèque',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Médiathèque',
        'new_item' => 'New Médiathèque',
        'edit_item' => 'Edit Médiathèque',
        'view_item' => 'View Médiathèque',
        'all_items' => 'All Médiathèque',
        'search_items' => 'Search Médiathèque',
        'parent_item_colon' => 'Parent Médiathèque:',
        'not_found' => 'No Médiathèque found.',
        'not_found_in_trash' => 'No Médiathèque found in Trash.'
    );

    $args = array(
        'public' => true,
        'labels' => $labels,
        'description' => 'My yummy Médiathèque will be published using this post type',
        'has_archive' => true,
        'supports' => array('title', 'editor', 'thumbnail'),
        'rewrite' => array('slug' => 'media')
    );
    register_post_type('mediatheque', $args);
}

function my_custom_post_collection()
{
    $labels = array(
        'name' => 'collection',
        'singular_name' => 'collection',
        'menu_name' => 'collection',
        'name_admin_bar' => 'collection',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New collection',
        'new_item' => 'New collection',
        'edit_item' => 'Edit collection',
        'view_item' => 'View collection',
        'all_items' => 'All collection',
        'search_items' => 'Search collection',
        'parent_item_colon' => 'Parent collection:',
        'not_found' => 'No collection found.',
        'not_found_in_trash' => 'No collection found in Trash.'
    );

    $args = array(
        'public' => true,
        'labels' => $labels,
        'description' => 'My yummy collection will be published using this post type',
        'has_archive' => true,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
        'rewrite' => array('slug' => 'collection')
    );
    register_post_type('collection', $args);
}

add_action('init', 'my_custom_post_collection');
add_action('init', 'my_custom_post_types');
function get_Date()
{
    return the_date('M,d,Y', '<span class="date_media">', '</span>');
}

add_shortcode('date_media', 'get_Date');


add_action('widgets_init', 'theme_slug_widgets_init2');
function theme_slug_widgets_init2()
{
    register_sidebar(array(
        'name' => __('widget 2', 'theme-slug'),
        'id' => 'sidebar-2',
        'description' => __('Widgets in this area will be shown on all posts and pages.', 'theme-slug'),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
    ));
}

function get_breadcrumb()
{

    if (is_post_type_archive('collection') || is_post_type_archive('mediatheque')) {
        echo '<a href="' . home_url() . '" rel="nofollow">Accueil</a>';

       // echo "&nbsp;&nbsp;&sol;&nbsp;&nbsp;";
       // echo get_post_type();
        the_category(' &bull; ');

    } elseif (is_page()) {
        echo '<a href="' . home_url() . '" rel="nofollow">Accueil</a>';
        //echo "&nbsp;&nbsp;&sol;&nbsp;&nbsp;";
        //echo the_title();
    } elseif (is_search()) {
        echo '<a href="' . home_url() . '" rel="nofollow">Accueil</a>';

    } elseif (is_singular('collection') || is_singular('mediatheque') ) {
        echo '<a  href="' . home_url() . '" rel="nofollow">Accueil</a>';
        echo "&nbsp;&nbsp;&sol;&nbsp;&nbsp;";
        echo '<span class = "home_single">'. get_post_type().'</span>';
    }
}
function misha_my_load_more_scripts() {

    global $wp_query2;
    global $post;
if (!is_page() && is_archive('mediatheque')){
    $args = array(
        post_type =>'mediatheque' ,
        posts_per_page => 6
    );
    $wp_query2 = new WP_Query( $args );

    // In most cases it is already included on the page and this line can be removed
    wp_enqueue_script('jquery');

    // register our main script but do not enqueue it yet
    wp_register_script( 'my_loadmore', get_template_directory_uri() . '/js/myloadmore.js', array('jquery') );

    // now the most interesting part
    // we have to pass parameters to myloadmore.js script but we can get the parameters values only in PHP
    // you can define variables directly in your HTML but I decided that the most proper way is wp_localize_script()
    wp_localize_script( 'my_loadmore', 'misha_loadmore_params', array(
        'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
        'posts' => serialize( $wp_query2->query_vars ), // everything about your loop is here
        'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
        'max_page' => $wp_query2->max_num_pages,

    ) );







    wp_enqueue_script( 'my_loadmore' );
}

}

add_action( 'wp_enqueue_scripts', 'misha_my_load_more_scripts' );
function misha_loadmore_ajax_handler(){

    // prepare our arguments for the query
    $args = unserialize( stripslashes( $_POST['query'] ) );

  $args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
   $args['post_status'] = 'publish';
    // it is always better to use WP_Query but not here


    $my_query3 = new WP_Query($args);

        if($my_query3->have_posts()) : while ($my_query3->have_posts() ) : $my_query3->the_post();
            get_template_part('template-parts/content', 'media');

            // If comments are open or we have at least one comment, load up the comment template.


        endwhile; // End of the loop.
        endif;


    die; // here we exit the script and even no wp_reset_query() required!
}



add_action('wp_ajax_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}

