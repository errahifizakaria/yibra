
<form role="search" method="get" id="searchform" action="<?php echo get_site_url(); ?>">
    <div class="input-field">

        <input type="text" value="" name="s" id="s"  />
        <label for="s">Search</label>
        <button type="submit"  id="searchsubmit" value="Submit"><i class="fa fa-search" aria-hidden="true"></i></button>
    </div>
</form>