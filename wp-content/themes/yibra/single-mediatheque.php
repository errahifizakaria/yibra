<?php

/**

 * The template for displaying all single posts

 *

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post

 *

 * @package yibra

 */



get_header('cover'); ?>



    <div id="primary" class="content-area">

        <main id="main" class="site-main container">



            <?php



            while ( have_posts() ) : the_post();



                get_template_part( 'template-parts/content', 'single-media' );











            endwhile; // End of the loop.

            ?>



        </main><!-- #main -->



        <div class="back_navigation back_navigation_media_single" style="">



            <div class="previous_navigation">

                <span><?php previous_post_link( '%link', 'PREV MEDIA' ); ?></span>

                <span> <?php previous_post_link('<strong>%link</strong>') ?></span>



            </div>

            <div class="container_separator_link">

                <span class="separator_link"></span>

            </div>

            <div class="next_navigation">

                <span><?php next_post_link( '%link', 'NEXT MEDIA' ); ?></span>

                <span><?php next_post_link('<strong>%link</strong>') ?></span>

            </div>

        </div>

    </div><!-- #primary -->



<?php

get_sidebar();

get_footer();

