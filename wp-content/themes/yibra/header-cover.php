<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package yibra
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>


    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Skip to content', 'yibra'); ?></a>

    <header id="masthead" <?php if(!empty(get_field('cover_site'))){echo 'style="background-image: url('.get_field("cover_site").')"' ;} ?>  class="site-header class_header <?php /*echo the_title()*/; if(is_post_type_archive('mediatheque')){echo 'class_mediatheque' ;}elseif(is_singular('mediatheque')){ echo 'class_single_mediatheque' ;}elseif(is_singular('collection') || is_post_type_archive('collection')){echo 'class_header_collection';}elseif (is_page('intsa-shop')){echo 'intsa_shop_header';}else{echo 'class_header';} ?>">
  
        <div class="site-branding">

<div class="menu-btn white_">
                <a class="btn-open" href="#"></a>

            </div>
          
            <a href="<?php echo get_site_url(); ?>" class="logo_yibra"> 
            <img src="<?php echo get_template_directory_uri() ?>/img/yibra_logo_white.svg" alt="yibra logo">
            <div class="">

            </div>
                        
                        </a>

            <div class="overlay">
                <div class="header_menu">

                    <div class="search_menu">

                        <?php include('form_search.php'); ?>
                        <?php //get_search_form(); ?>
                    </div>
                    <div class="social_menu social">
                        <?php include('social.php'); ?>
                    </div>
                </div>


                <div class="menu">

                    <nav id="site-navigation" class="main-navigation">
                        <!--                        <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">-->
                        <?php //esc_html_e( 'Primary Menu', 'yibra' ); ?><!--</button>-->
                        <?php
                        wp_nav_menu(array(
                            'theme_location' => 'menu-1',
                            'menu_id' => 'primary-menu',
                        ));
                        ?>
                    </nav><!-- #site-navigation -->
                </div>
            </div>
            <div class="social socialtop white_">
                <ul>
                    <li><a target="_blank" href="<?php echo get_option('twitter_link'); ?>"><i class="fa fa-twitter"
                                                                               aria-hidden="true"></i></a></li>
                    <li><a target="_blank" href="<?php echo get_option('instagram_link'); ?>"><i class="fa fa-instagram"
                                                                                 aria-hidden="true"></i></a></li>
                    <li><a target="_blank" href="<?php echo get_option('facebook_link'); ?>"><i class="fa fa-facebook"
                                                                                aria-hidden="true"></i></a></li>
                    <li><a target="_blank" href="<?php echo get_option('youtube_link'); ?>"><i class="fa fa-youtube-play"
                                                                               aria-hidden="true"></i></a></li>

                </ul>
            </div>
        </div><!-- .site-branding -->
        <?php if(!is_singular('collection') ){ ?>
        <div class="cover_with_link">
            <div class="breadcrumb"><?php get_breadcrumb(); ?></div>

            <div class="container_title_custom_yp2">
                <div class="title_custom_yp2">
                <span class=" motif  motif1"></span>
                <span class=" motif motif2"></span>
                <span class=" motif  motif3"></span>
                <span class=" motif motif4"></span>
                </div>
                <?php
                if (is_post_type_archive('mediatheque')) {
                    echo "<span class='center_text'>";
                    echo 'mediathéque';
                    echo "</span>";
                } elseif (is_post_type_archive('collection')){
                    echo "<span class='center_text sdsdqs '>";
                    echo 'collection' ;
                    echo "</span>";
                }elseif (is_search()){
                    echo "<span class='center_text'>";
                    echo 'Recherche';
                    echo "</span>";

                }else {
                 echo "<span class='center_text '>";
                 echo str_word_count(get_the_title()) > 2  ?  wp_trim_words( get_the_title(), 2 ):  get_the_title() ;
                    echo "</span>";

                }


                ?>
            </div>
        </div>
        <?php  }?>
    </header><!-- #masthead -->

    <div id="content" class="site-content">
