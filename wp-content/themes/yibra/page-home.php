<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="sliderHome" id="sliderHome">

            <?php
            $loop = new WP_Query(array('post_type' => 'slider', 'posts_per_page' => 10));
            echo '<div class="slider-for">';

            while ($loop->have_posts()) : $loop->the_post(); ?>
                <div class="slider-for-items ">
                    <a class="desktop" href="javascript:void(0);"><?= the_title(); ?></a>
                    <a class="mobile  "
                       href="javascript:void(0);"><?php echo strlen(get_the_title()) > 2 ? substr(get_the_title(), 0, 1) : get_the_title() ?></a>

                </div>
            <?php endwhile;
            echo '</div>';
            echo '<div class="slider-nav">';
            while ($loop->have_posts()) : $loop->the_post(); ?>

                <div class="slider-nav-items">
                    <div class="info_slider">


                        <h2 class="yb_title1"><?php the_field('slide_home_title_1') ?></h2>
                        <h2 class="yb_title2"><?php the_field('slide_home_title_2') ?></h2>
                        <img class="img_center" src="<?php the_field('image_slider') ?>" class="" alt="">
                        <div class="img_text">

                            <div class="text_see">
                                <?php the_field('text_description') ?>
                                <a href=" <?php the_field('link') ?>" class="btn_see_all"> voir la galerie </a>
                            </div>

                            <!-- <div class="yb_desc">

                             </div>
                             <div class="">

                             </div>-->
                        </div>

                    </div>
                </div>
            <?php endwhile;
            echo '</div>';

            ?>

        </div>
        <?php
        // Start the loop.
        while (have_posts()) : the_post();

            // Include the page content template.
            get_template_part('template-parts/content', 'page');


            // End of the loop.
        endwhile;
        ?>

    </main><!-- .site-main -->

    <?php //get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->


<?php get_footer(); ?>
