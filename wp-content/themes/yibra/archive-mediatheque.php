<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package yibra
 */

get_header('cover'); ?>

    <div id="primary" class="content-area">
        <main id="media" class="site-main container media">

            <div class="row">
            <?php
            $args = array( 'post_type' => 'mediatheque','posts_per_page' => 6 );
            $my_query = new WP_Query($args);
            //$query = new WP_Query( $arg );
        if($my_query->have_posts()) : while ($my_query->have_posts() ) : $my_query->the_post();
                get_template_part('template-parts/content', 'media');

                // If comments are open or we have at least one comment, load up the comment template.


            endwhile; // End of the loop.
            endif;
            wp_reset_postdata();
            ?>



            </div>
        </main><!-- #main -->


    </div><!-- #primary -->
    <div><a href="#" class="misha_loadmore">+</a></div>
<?php
//get_sidebar();
get_footer();
