
    <ul>
        <li> <a target="_blank"  href="<?php echo get_option('twitter_link'); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li> <a target="_blank" href="<?php echo get_option('instagram_link'); ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
        <li> <a target="_blank" href="<?php echo get_option('facebook_link'); ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li> <a target="_blank" href="<?php echo get_option('youtube_link'); ?>"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>

    </ul>
