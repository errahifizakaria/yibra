<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package yibra
 */

?>

<div class="col s12 m12 l6">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <div class="container_post_thumbnail_media">

            <div class="post_thumbnail_media">
                <a href="<?php echo get_permalink(); ?>">
                    <?php the_post_thumbnail(); ?>
                </a>
            </div>
            <div class="post_thumbnail_media_title">
                    <span>
                        <?php
                        switch (get_field('type_media')) {
                            case 'video':
                                echo ' <img src="' . get_template_directory_uri() . '/img/movie.png" >';
                                break;
                            case 'image':
                                echo ' <img src="' . get_template_directory_uri() . '/img/image.png" >';
                                break;
                            case 'document':
                                echo ' <img src="' . get_template_directory_uri() . '/img/document.png" >';
                                break;
                            default:

                        }


                        ?>

                                </span> <?php the_title(); ?>


            </div>


        </div><!-- .entry-content -->

    </article><!-- #post-<?php the_ID(); ?> -->
</div>
