<?php

/**

 * Template part for displaying posts

 *

 * @link https://codex.wordpress.org/Template_Hierarchy

 *

 * @package yibra

 */



?>



<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>



    <header class="entry-header">



    </header><!-- .entry-header -->



    <div class="entry-content">

        <div class="video_media">

            <?php echo the_field('video_media') ?>

        </div>

        <div class="text_and_icon post_thumbnail_media_title">



                        <?php

                        switch (get_field('type_media')) {

                            case 'video':

                                echo ' <i class="fa fa-film" aria-hidden="true"></i>';

                                break;

                            case 'image':

                                echo ' <i class="fa fa-file-image-o" aria-hidden="true"></i>';

                                break;

                            case 'document':

                                echo ' <i class="fa fa-file-text-o" aria-hidden="true"></i>';

                                break;

                            default:



                        }

                        ?>





            <div class="title_media">

                <?php echo the_title() ?>

                <?php echo the_date('M d,Y', '<span class="date_post">', '</span>'); ?>

            </div>

        </div>

        <div class="text_media">

            <?php the_content(); ?>

        </div>





    </div><!-- .entry-content -->



    <footer class="entry-footer">



    </footer><!-- .entry-footer -->

</article><!-- #post-<?php the_ID(); ?> -->

