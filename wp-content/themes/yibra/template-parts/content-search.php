<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package yibra
 */

?>

<article  id="post-<?php the_ID(); ?> class=" serach_post">
<div class="all_result animated slideInDown">


	<header class="entry-header  ">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php yibra_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

    <a href="<?php echo get_permalink(); ?>">
        <?php the_post_thumbnail(); ?>
    </a>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->

	<footer class="entry-footer">
		<?php yibra_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</div>
</article><!-- #post-<?php the_ID(); ?> -->
