<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package yibra
 */

?>

<div class="collection_item ">
    <div class="container_collection_item">
        <div class="back_collection_item_lg show-on-large hide-on-med-and-down" style="background-image: url(<?php echo the_field('background_collection2');?>)">

        </div>
        <div class="back_collection_item_md show-on-medium hide-on-small-only hide-on-large-only" style="background-image: url(<?php echo the_field('background_collection_mini_desk');?>)">

        </div>
        <div class="back_collection_item_sm show-on-small hide-on-large-only hide-on-med-only" style="background-image: url(<?php echo the_field('background_collection_mobile');?>)">

        </div>

        <div class="title_collection_item">
            <div class="inner"></div>
            <div class="logo_collection_item">
                <img src="<?php echo the_field('logo_collection');?>" alt="">

            </div>
            <div class="exprect">
                <?php echo get_the_excerpt(); ?>

            </div>
            <div class="btn_collection">
                <a href="<?php echo get_permalink(); ?>" class="btn_collection_link"> Voir cette collection </a>
            </div>
        </div>
        
    </div>
</div>

