<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package yibra
 */

?>

<article id="post-<?php the_ID(); ?>" >


    <div class="">


        <?php the_content(); ?>

    </div><!-- .entry-content -->

    <footer class="entry-footer">

    </footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
