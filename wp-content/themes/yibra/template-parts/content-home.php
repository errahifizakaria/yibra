<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package yibra
 */

?>
<div class="sliderHome">

    <?php
    $loop = new WP_Query(array('post_type' => 'slider', 'posts_per_page' => 10));
    echo '<div class="slider-for">';

    while ($loop->have_posts()) : $loop->the_post(); ?>
        <div class="slider-for-items"><a href="javascript:void(0);"><?=the_title();?></a></div>
    <?php endwhile;
    echo '</div>';
    echo '<div class="slider-nav">';
    while ($loop->have_posts()) : $loop->the_post(); ?>
        <div class="slider-nav-items"><?=$loop->get_the_content();?></div>
    <?php endwhile;
    echo '</div>';

    ?>

</div>


