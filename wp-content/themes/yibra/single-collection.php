<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package yibra
 */

get_header('collection'); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main ">

            <?php

            while ( have_posts() ) : the_post();

                get_template_part( 'template-parts/content', 'single-collection' );

            endwhile; // End of the loop.
            ?>

        </main><!-- #main -->

        <div class="  <?php if( !empty(get_field('navigation_link'))){  echo 'back_navigation_custom'; } else{ echo 'back_navigation_collection';}?>"  style = " background-image: url( <?php if( !empty(get_field('navigation_link'))){  echo get_field('navigation_link' ); } else{ echo get_template_directory_uri()."/img/collection/single_collection1_bottom.jpg";}?> )" >


            <div class="previous_navigation">
                <span>PREV <?php echo get_post_type(); ?></span>
                <span> <?php previous_post_link('<strong>%link</strong>') ?></span>

            </div>
            <div class="container_separator_link">
                <span class="separator_link"></span>
            </div>
            <div class="next_navigation">
                <span>NEXT <?php echo get_post_type(); ?></span>
                <span><?php next_post_link('<strong>%link</strong>') ?></span>
            </div>
        </div>
    </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
