<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package yibra
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>


    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'yibra' ); ?></a>

	<header id="masthead" class="site-header">

		<div class="site-branding">


            <div class="menu-btn ">
                <a class="btn-open" href="#"></a>

            </div>
          <a href="<?php echo get_site_url(); ?>" class="logo_yibra"> <img src="<?php echo get_template_directory_uri()?>/img/yibra_logo_black.svg " alt="yibra logo"></a>

            <div class="overlay">
                <div class="header_menu">

                    <div class="search_menu">

                        <?php include ('form_search.php');?>
                        <?php //get_search_form(); ?>
                    </div>
                    <div class="social_menu social">
                        <?php include ('social.php');?>
                    </div>
                </div>


                <div class="menu">

                    <nav id="site-navigation" class="main-navigation">
<!--                        <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">--><?php //esc_html_e( 'Primary Menu', 'yibra' ); ?><!--</button>-->
                        <?php
                        wp_nav_menu( array(
                            'theme_location' => 'menu-1',
                            'menu_id'        => 'primary-menu',
                        ) );
                        ?>
                    </nav><!-- #site-navigation -->
                </div>
            </div>
            <div class="social socialtop">
                <?php include ('social.php');?>
            </div>
		</div><!-- .site-branding -->

	</header><!-- #masthead -->

	<div id="content" class="site-content">
