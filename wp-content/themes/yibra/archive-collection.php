<?php

/**

 * The template for displaying all pages

 *

 * This is the template that displays all pages by default.

 * Please note that this is the WordPress construct of pages

 * and that other 'pages' on your WordPress site may use a

 * different template.

 *

 * @link https://codex.wordpress.org/Template_Hierarchy

 *

 * @package yibra

 */



get_header('cover'); ?>



    <div id="primary" class="content-area">

        <main id="collection" class="site-main container collection">

            <div class="row">

            <?php

            $arg = array( 'post_type' => 'collection' );



            $query = new WP_Query( $arg );



            while (have_posts($query )) : the_post();

                get_template_part('template-parts/content', 'collection');



                // If comments are open or we have at least one comment, load up the comment template.





            endwhile; // End of the loop.

            ?>







            </div>

        </main><!-- #main -->





    </div><!-- #primary -->



<?php

//get_sidebar();

get_footer();

