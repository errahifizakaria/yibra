<?php

/* settings/basics.html */
class __TwigTemplate_31447355f72c21e12ff924a08df6f77161ad60850f01de1b364b08c422e8a976 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table class=\"form-table\">
  <tbody>
    <tr>
      <th scope=\"row\">
        <label for=\"settings[from_name]\">
          ";
        // line 6
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Default sender");
        echo "
        </label>
        <p class=\"description\">
          ";
        // line 9
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("These email addresses will be selected by default for each new email.");
        echo "
        </p>
      </th>
      <td>
        <!-- default from name & email -->
        <p>
          <label for=\"settings[from_name]\">";
        // line 15
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("From");
        echo "</label>
          <input type=\"text\"
            id=\"settings[from_name]\"
            name=\"sender[name]\"
            value=\"";
        // line 19
        if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_settings_, "sender", array()), "name", array()), "html", null, true);
        echo "\"
            placeholder=\"";
        // line 20
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Your name");
        echo "\" />
          <input type=\"email\"
            id=\"settings[from_email]\"
            name=\"sender[address]\"
            value=\"";
        // line 24
        if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_settings_, "sender", array()), "address", array()), "html", null, true);
        echo "\"
            placeholder=\"from@mydomain.com\" />
        </p>
        <!-- default reply_to name & email -->
        <p>
          <label for=\"settings[reply_name]\">";
        // line 29
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Reply-to");
        echo "</label>
          <input type=\"text\"
            id=\"settings[reply_name]\"
            name=\"reply_to[name]\"
            value=\"";
        // line 33
        if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_settings_, "reply_to", array()), "name", array()), "html", null, true);
        echo "\"
            placeholder=\"";
        // line 34
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Your name");
        echo "\" />
          <input type=\"email\"
            id=\"settings[reply_email]\"
            name=\"reply_to[address]\"
            value=\"";
        // line 38
        if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_settings_, "reply_to", array()), "address", array()), "html", null, true);
        echo "\"
            placeholder=\"reply_to@mydomain.com\" />
        </p>
      </td>
    </tr>
    <!-- email addresses receiving notifications -->
    <tr style=\"display:none\">
      <th scope=\"row\">
        <label for=\"settings[notification_email]\">
          ";
        // line 47
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Email notifications");
        echo "
        </label>
        <p class=\"description\">
          ";
        // line 50
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("These email addresses will receive notifications (separate each address with a comma).");
        echo "
        </p>
      </th>
      <td>
        <p>
          <input type=\"text\"
            id=\"settings[notification_email]\"
            name=\"notification[address]\"
            value=\"";
        // line 58
        if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_settings_, "notification", array()), "address", array()), "html", null, true);
        echo "\"
            placeholder=\"notification@mydomain.com\"
            class=\"regular-text\" />
        </p>
        <p>
          <label for=\"settings[notification_on_subscribe]\">
            <input type=\"checkbox\" id=\"settings[notification_on_subscribe]\"
            name=\"notification[on_subscribe]\"
            value=\"1\"
            ";
        // line 67
        if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
        if ($this->getAttribute($this->getAttribute($_settings_, "notification", array()), "on_subscribe", array())) {
            echo "checked=\"checked\"";
        }
        echo " />
            ";
        // line 68
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("When someone subscribes");
        echo "
          </label>
        </p>
        <p>
          <label for=\"settings[notification_on_unsubscribe]\">
            <input type=\"checkbox\"
            id=\"settings[notification_on_unsubscribe]\"
            name=\"notification[on_unsubscribe]\"
            value=\"1\"
            ";
        // line 77
        if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
        if ($this->getAttribute($this->getAttribute($_settings_, "notification", array()), "on_unsubscribe", array())) {
            echo "checked=\"checked\"";
        }
        echo " />
            ";
        // line 78
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("When someone unsubscribes");
        echo "
          </label>
        </p>
      </td>
    </tr>
    <!-- ability to subscribe in comments -->
    <!-- TODO: Check if registration is enabled (if not, display a message and disable setting) -->
    <tr>
      <th scope=\"row\">
        <label for=\"settings[subscribe_on_comment]\">
          ";
        // line 88
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Subscribe in comments");
        echo "
        </label>
        <p class=\"description\">
          ";
        // line 91
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Visitors that comment on a post can subscribe to your list via a checkbox.");
        echo "
        </p>
      </th>
      <td>
        <p>
          <input
            data-toggle=\"mailpoet_subscribe_on_comment\"
            type=\"checkbox\"
            value=\"1\"
            id=\"settings[subscribe_on_comment]\"
            name=\"subscribe[on_comment][enabled]\"
            ";
        // line 102
        if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
        if ($this->getAttribute($this->getAttribute($this->getAttribute($_settings_, "subscribe", array()), "on_comment", array()), "enabled", array())) {
            echo "checked=\"checked\"";
        }
        // line 103
        echo "          />
        </p>
        <div id=\"mailpoet_subscribe_on_comment\">
          <p>
            <input
              type=\"text\"
              id=\"settings[subscribe_on_comment_label]\"
              name=\"subscribe[on_comment][label]\"
              class=\"regular-text\"
              ";
        // line 112
        if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
        if ($this->getAttribute($this->getAttribute($this->getAttribute($_settings_, "subscribe", array()), "on_comment", array()), "label", array())) {
            // line 113
            echo "                  value=\"";
            if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_settings_, "subscribe", array()), "on_comment", array()), "label", array()), "html", null, true);
            echo "\"
              ";
        } else {
            // line 115
            echo "                value=\"";
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Yes, add me to your mailing list");
            echo "\"
              ";
        }
        // line 117
        echo "            />
          </p>
          <p>
            <label>";
        // line 120
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Users will be subscribed to these lists:");
        echo "</label>
          </p>
          <p>
            <select
              id=\"mailpoet_subscribe_on_comment_segments\"
              name=\"subscribe[on_comment][segments][]\"
              data-placeholder=\"";
        // line 126
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Choose a list");
        echo "\"
              multiple
            >
              ";
        // line 129
        if (isset($context["segments"])) { $_segments_ = $context["segments"]; } else { $_segments_ = null; }
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($_segments_);
        foreach ($context['_seq'] as $context["_key"] => $context["segment"]) {
            // line 130
            echo "                <option
                  value=\"";
            // line 131
            if (isset($context["segment"])) { $_segment_ = $context["segment"]; } else { $_segment_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_segment_, "id", array()), "html", null, true);
            echo "\"
                  ";
            // line 132
            if (isset($context["segment"])) { $_segment_ = $context["segment"]; } else { $_segment_ = null; }
            if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
            if (twig_in_filter($this->getAttribute($_segment_, "id", array()), $this->getAttribute($this->getAttribute($this->getAttribute($_settings_, "subscribe", array()), "on_comment", array()), "segments", array()))) {
                // line 133
                echo "                    selected=\"selected\"
                  ";
            }
            // line 135
            echo "                >";
            if (isset($context["segment"])) { $_segment_ = $context["segment"]; } else { $_segment_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_segment_, "name", array()), "html", null, true);
            echo " (";
            if (isset($context["segment"])) { $_segment_ = $context["segment"]; } else { $_segment_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_segment_, "subscribers", array()), "html", null, true);
            echo ")</option>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['segment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 137
        echo "            </select>
          </p>
        </div>
      </td>
    </tr>
    <!-- ability to subscribe when registering -->
    <!-- TODO: Only available for the main site of a multisite! -->
    <!-- TODO: Check if registration is enabled (if not, display a message and disable setting) -->
    <tr>
      <th scope=\"row\">
        <label for=\"settings[subscribe_on_register]\">
          ";
        // line 148
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Subscribe in registration form");
        echo "
        </label>
        <p class=\"description\">
          ";
        // line 151
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Allow users who register as a WordPress user on your website to subscribe to a MailPoet list (in addition to the \"WordPress Users\" list).");
        echo "
        </p>
      </th>
      <td>
        ";
        // line 155
        if (isset($context["flags"])) { $_flags_ = $context["flags"]; } else { $_flags_ = null; }
        if (($this->getAttribute($_flags_, "registration_enabled", array()) == true)) {
            // line 156
            echo "          <p>
            <input
              data-toggle=\"mailpoet_subscribe_in_form\"
              type=\"checkbox\"
              value=\"1\"
              id=\"settings[subscribe_on_register]\"
              name=\"subscribe[on_register][enabled]\"
              ";
            // line 163
            if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
            if ($this->getAttribute($this->getAttribute($this->getAttribute($_settings_, "subscribe", array()), "on_register", array()), "enabled", array())) {
                // line 164
                echo "                checked=\"checked\"
              ";
            }
            // line 166
            echo "            />
          </p>

          <div id=\"mailpoet_subscribe_in_form\">
            <p>
              <input
                type=\"text\"
                id=\"settings[subscribe_on_register_label]\"
                name=\"subscribe[on_register][label]\"
                class=\"regular-text\"
                ";
            // line 176
            if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
            if ($this->getAttribute($this->getAttribute($this->getAttribute($_settings_, "subscribe", array()), "on_register", array()), "label", array())) {
                // line 177
                echo "                  value=\"";
                if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_settings_, "subscribe", array()), "on_register", array()), "label", array()), "html", null, true);
                echo "\"
                ";
            } else {
                // line 179
                echo "                  value=\"";
                echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Yes, add me to your mailing list");
                echo "\"
                ";
            }
            // line 181
            echo "              />
            </p>
            <p>
              <label>";
            // line 184
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Users will be subscribed to these lists:");
            echo "</label>
            </p>
            <p>
              <select
                id=\"mailpoet_subscribe_on_register_segments\"
                name=\"subscribe[on_register][segments][]\"
                data-placeholder=\"";
            // line 190
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Choose a list");
            echo "\"
                multiple
              >
                ";
            // line 193
            if (isset($context["segments"])) { $_segments_ = $context["segments"]; } else { $_segments_ = null; }
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($_segments_);
            foreach ($context['_seq'] as $context["_key"] => $context["segment"]) {
                // line 194
                echo "                  <option
                    value=\"";
                // line 195
                if (isset($context["segment"])) { $_segment_ = $context["segment"]; } else { $_segment_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_segment_, "id", array()), "html", null, true);
                echo "\"
                    ";
                // line 196
                if (isset($context["segment"])) { $_segment_ = $context["segment"]; } else { $_segment_ = null; }
                if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
                if (twig_in_filter($this->getAttribute($_segment_, "id", array()), $this->getAttribute($this->getAttribute($this->getAttribute($_settings_, "subscribe", array()), "on_register", array()), "segments", array()))) {
                    // line 197
                    echo "                      selected=\"selected\"
                    ";
                }
                // line 199
                echo "                  >";
                if (isset($context["segment"])) { $_segment_ = $context["segment"]; } else { $_segment_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_segment_, "name", array()), "html", null, true);
                echo " (";
                if (isset($context["segment"])) { $_segment_ = $context["segment"]; } else { $_segment_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_segment_, "subscribers", array()), "html", null, true);
                echo ")</option>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['segment'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 201
            echo "              </select>
            </p>
          </div>
        ";
        } else {
            // line 205
            echo "          <p>
            <em>";
            // line 206
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Registration is disabled on this site.");
            echo "</em>
          </p>
        ";
        }
        // line 209
        echo "      </td>
    </tr>
    <!-- edit subscription-->
    <tr>
      <th scope=\"row\">
        <label for=\"subscription_manage_page\">
          ";
        // line 215
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Manage Subscription page");
        echo "
        </label>
        <p class=\"description\">
          ";
        // line 218
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("When your subscribers click the \"Manage your subscription\" link, they will be directed to this page.");
        echo "
          <br />
          ";
        // line 220
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("If you want to use a custom Subscription page, simply paste this shortcode on to a WordPress page: [mailpoet_manage_subscription]");
        echo "
        </p>
      </th>
      <td>
        <p>
          <select
            class=\"mailpoet_page_selection\"
            id=\"subscription_manage_page\"
            name=\"subscription[pages][manage]\"
          >
            ";
        // line 230
        if (isset($context["pages"])) { $_pages_ = $context["pages"]; } else { $_pages_ = null; }
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($_pages_);
        foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
            // line 231
            echo "              <option
                value=\"";
            // line 232
            if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_page_, "id", array()), "html", null, true);
            echo "\"
                data-preview-url=\"";
            // line 233
            if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
            echo $this->getAttribute($this->getAttribute($_page_, "url", array()), "manage", array());
            echo "\"
                ";
            // line 234
            if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
            if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
            if (($this->getAttribute($_page_, "id", array()) == $this->getAttribute($this->getAttribute($this->getAttribute($_settings_, "subscription", array()), "pages", array()), "manage", array()))) {
                // line 235
                echo "                  selected=\"selected\"
                ";
            }
            // line 237
            echo "              >";
            if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_page_, "title", array()), "html", null, true);
            echo "</option>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 239
        echo "          </select>
          <a
            class=\"mailpoet_page_preview\"
            href=\"javascript:;\"
            title=\"";
        // line 243
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Preview page");
        echo "\"
          >";
        // line 244
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Preview");
        echo "</a>
        </p>
        <p>
          <label>";
        // line 247
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Subscribers can choose from these lists:");
        echo "</label>
        </p>
        <p>
          <select
            id=\"mailpoet_subscription_edit_segments\"
            name=\"subscription[segments][]\"
            data-placeholder=\"";
        // line 253
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Leave this field empty to display all lists");
        echo "\"
            multiple
          >
            ";
        // line 256
        if (isset($context["segments"])) { $_segments_ = $context["segments"]; } else { $_segments_ = null; }
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($_segments_);
        foreach ($context['_seq'] as $context["_key"] => $context["segment"]) {
            // line 257
            echo "              <option
                value=\"";
            // line 258
            if (isset($context["segment"])) { $_segment_ = $context["segment"]; } else { $_segment_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_segment_, "id", array()), "html", null, true);
            echo "\"
                ";
            // line 259
            if (isset($context["segment"])) { $_segment_ = $context["segment"]; } else { $_segment_ = null; }
            if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
            if (twig_in_filter($this->getAttribute($_segment_, "id", array()), $this->getAttribute($this->getAttribute($_settings_, "subscription", array()), "segments", array()))) {
                // line 260
                echo "                  selected=\"selected\"
                ";
            }
            // line 262
            echo "              >";
            if (isset($context["segment"])) { $_segment_ = $context["segment"]; } else { $_segment_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_segment_, "name", array()), "html", null, true);
            echo " (";
            if (isset($context["segment"])) { $_segment_ = $context["segment"]; } else { $_segment_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_segment_, "subscribers", array()), "html", null, true);
            echo ")</option>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['segment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 264
        echo "          </select>
        </p>
      </td>
    </tr>
    <!-- unsubscribe-->
    <tr>
      <th scope=\"row\">
        <label for=\"subscription_unsubscribe_page\">
          ";
        // line 272
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Unsubscribe page");
        echo "
        </label>
        <p class=\"description\">
          ";
        // line 275
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("When your subscribers click the \"Unsubscribe\" link, they will be directed to this page.");
        echo "
          <br />
          ";
        // line 277
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("If you want to use a custom Unsubscribe page, simply paste this shortcode on to a WordPress page: [mailpoet_manage text=\"Manage your subscription\"]");
        echo "
        </p>
      </th>
      <td>
        <p>
          <select
            class=\"mailpoet_page_selection\"
            id=\"subscription_unsubscribe_page\"
            name=\"subscription[pages][unsubscribe]\"
          >
            ";
        // line 287
        if (isset($context["pages"])) { $_pages_ = $context["pages"]; } else { $_pages_ = null; }
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($_pages_);
        foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
            // line 288
            echo "              <option
                value=\"";
            // line 289
            if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_page_, "id", array()), "html", null, true);
            echo "\"
                data-preview-url=\"";
            // line 290
            if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
            echo $this->getAttribute($this->getAttribute($_page_, "url", array()), "unsubscribe", array());
            echo "\"
                ";
            // line 291
            if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
            if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
            if (($this->getAttribute($_page_, "id", array()) == $this->getAttribute($this->getAttribute($this->getAttribute($_settings_, "subscription", array()), "pages", array()), "unsubscribe", array()))) {
                // line 292
                echo "                  selected=\"selected\"
                ";
            }
            // line 294
            echo "              >";
            if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_page_, "title", array()), "html", null, true);
            echo "</option>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 296
        echo "          </select>
          <a
            class=\"mailpoet_page_preview\"
            href=\"javascript:;\"
            title=\"";
        // line 300
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Preview page");
        echo "\"
          >";
        // line 301
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Preview");
        echo "</a>
        </p>
      </td>
    </tr>
    <!-- shortcode: archive page  -->
    <tr>
      <th scope=\"row\">
        <label>
          ";
        // line 309
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Archive page shortcode");
        echo "
        </label>
        <p class=\"description\">
          ";
        // line 312
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Paste this shortcode on a page to display a list of past newsletters.");
        echo "
        </p>
      </th>
      <td>
        <p>
          <input
            type=\"text\"
            class=\"regular-text\"
            id=\"mailpoet_shortcode_archives\"
            value=\"[mailpoet_archive]\"
            onClick=\"this.focus();this.select();\"
            readonly=\"readonly\"
          />
        </p>
        <p>
          <select
            id=\"mailpoet_shortcode_archives_list\"
            data-shortcode=\"mailpoet_archive\"
            data-output=\"mailpoet_shortcode_archives\"
            data-placeholder=\"";
        // line 331
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Leave this field empty to display all lists");
        echo "\"
            multiple
          >
            ";
        // line 334
        if (isset($context["segments"])) { $_segments_ = $context["segments"]; } else { $_segments_ = null; }
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($_segments_);
        foreach ($context['_seq'] as $context["_key"] => $context["segment"]) {
            // line 335
            echo "              <option value=\"";
            if (isset($context["segment"])) { $_segment_ = $context["segment"]; } else { $_segment_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_segment_, "id", array()), "html", null, true);
            echo "\">";
            if (isset($context["segment"])) { $_segment_ = $context["segment"]; } else { $_segment_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_segment_, "name", array()), "html", null, true);
            echo " (";
            if (isset($context["segment"])) { $_segment_ = $context["segment"]; } else { $_segment_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_segment_, "subscribers", array()), "html", null, true);
            echo ")</option>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['segment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 337
        echo "          </select>
        </p>
      </td>
    </tr>
    <!-- shortcode: total number of subscribers -->
    <tr>
      <th scope=\"row\">
        <label>
          ";
        // line 345
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Shortcode to display total number of subscribers");
        echo "
        </label>
        <p class=\"description\">
          ";
        // line 348
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Paste this shortcode on a post or page to display the total number of confirmed subscribers.");
        echo "
        </p>
      </th>
      <td>
        <p>
          <input
            type=\"text\"
            class=\"regular-text\"
            id=\"mailpoet_shortcode_subscribers\"
            value=\"[mailpoet_subscribers_count]\"
            onClick=\"this.focus();this.select();\"
            readonly=\"readonly\"
          />
        </p>
        <p>
          <select
            id=\"mailpoet_shortcode_subscribers_count\"
            data-shortcode=\"mailpoet_subscribers_count\"
            data-output=\"mailpoet_shortcode_subscribers\"
            data-placeholder=\"";
        // line 367
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Leave this field empty to display all lists");
        echo "\"
            multiple
          >
            ";
        // line 370
        if (isset($context["segments"])) { $_segments_ = $context["segments"]; } else { $_segments_ = null; }
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($_segments_);
        foreach ($context['_seq'] as $context["_key"] => $context["segment"]) {
            // line 371
            echo "              <option value=\"";
            if (isset($context["segment"])) { $_segment_ = $context["segment"]; } else { $_segment_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_segment_, "id", array()), "html", null, true);
            echo "\">";
            if (isset($context["segment"])) { $_segment_ = $context["segment"]; } else { $_segment_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_segment_, "name", array()), "html", null, true);
            echo " (";
            if (isset($context["segment"])) { $_segment_ = $context["segment"]; } else { $_segment_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_segment_, "subscribers", array()), "html", null, true);
            echo ")</option>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['segment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 373
        echo "          </select>
        </p>
      </td>
    </tr>
  </tbody>
</table>

<script type=\"text/javascript\">
  jQuery(function(\$) {
    // on dom loaded
    \$(function() {
      // select2 instances
      \$('#mailpoet_subscribe_on_comment_segments').select2();
      \$('#mailpoet_subscribe_on_register_segments').select2();
      \$('#mailpoet_subscription_edit_segments').select2();
      \$('#mailpoet_shortcode_archives_list').select2();
      \$('#mailpoet_shortcode_subscribers_count').select2();
      // fix lengthy placeholder from being cut off by select 2
      \$('.select2-search__field').each(function() {
        \$(this).css('width', (\$(this).attr('placeholder').length * 0.75) + 'em');
      });

      // shortcodes
      \$('#mailpoet_shortcode_archives_list, #mailpoet_shortcode_subscribers_count')
      .on('change', function() {
        var shortcode = \$(this).data('shortcode'),
          values = \$(this).val() || [];

        if (values.length > 0) {
          shortcode += ' segments=\"';
          shortcode += values.join(',');
          shortcode += '\"';
        }

        \$('#' + \$(this).data('output'))
          .val('[' + shortcode + ']');
      });
    });
  });
</script>
";
    }

    public function getTemplateName()
    {
        return "settings/basics.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  762 => 373,  746 => 371,  741 => 370,  735 => 367,  713 => 348,  707 => 345,  697 => 337,  681 => 335,  676 => 334,  670 => 331,  648 => 312,  642 => 309,  631 => 301,  627 => 300,  621 => 296,  611 => 294,  607 => 292,  603 => 291,  598 => 290,  593 => 289,  590 => 288,  585 => 287,  572 => 277,  567 => 275,  561 => 272,  551 => 264,  538 => 262,  534 => 260,  530 => 259,  525 => 258,  522 => 257,  517 => 256,  511 => 253,  502 => 247,  496 => 244,  492 => 243,  486 => 239,  476 => 237,  472 => 235,  468 => 234,  463 => 233,  458 => 232,  455 => 231,  450 => 230,  437 => 220,  432 => 218,  426 => 215,  418 => 209,  412 => 206,  409 => 205,  403 => 201,  390 => 199,  386 => 197,  382 => 196,  377 => 195,  374 => 194,  369 => 193,  363 => 190,  354 => 184,  349 => 181,  343 => 179,  336 => 177,  333 => 176,  321 => 166,  317 => 164,  314 => 163,  305 => 156,  302 => 155,  295 => 151,  289 => 148,  276 => 137,  263 => 135,  259 => 133,  255 => 132,  250 => 131,  247 => 130,  242 => 129,  236 => 126,  227 => 120,  222 => 117,  216 => 115,  209 => 113,  206 => 112,  195 => 103,  190 => 102,  176 => 91,  170 => 88,  157 => 78,  150 => 77,  138 => 68,  131 => 67,  118 => 58,  107 => 50,  101 => 47,  88 => 38,  81 => 34,  76 => 33,  69 => 29,  60 => 24,  53 => 20,  48 => 19,  41 => 15,  32 => 9,  26 => 6,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "settings/basics.html", "/home/yibraenvolgroupe/public_html/wp-content/plugins/mailpoet/views/settings/basics.html");
    }
}
