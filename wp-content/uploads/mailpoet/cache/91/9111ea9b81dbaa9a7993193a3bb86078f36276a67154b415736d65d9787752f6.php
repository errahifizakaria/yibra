<?php

/* settings/signup.html */
class __TwigTemplate_ffdfb95feceee610eaf41e3d81be8de0908a300cb1df430d785dc21ea25b6abf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table class=\"form-table\">
  <tbody>
    <!-- enable sign-up confirmation -->
    <tr>
      <th scope=\"row\">
        <label>
          ";
        // line 7
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Enable sign-up confirmation");
        echo "
        </label>
        <p class=\"description\">
          ";
        // line 10
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("If you enable this option, your subscribers will first receive a confirmation email after they subscribe. Once they confirm their subscription (via this email), they will be marked as 'confirmed' and will begin to receive your email newsletters.");
        echo "
          <a href=\"http://docs.mailpoet.com/article/128-why-you-should-use-signup-confirmation-double-opt-in\" target=\"_blank\">";
        // line 11
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Read more about Double Opt-in confirmation.");
        echo "</a>
        </p>
      </th>
      <td>
        <p
          id=\"mailpoet_signup_confirmation_notice\"
          ";
        // line 17
        if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
        if (($this->getAttribute($_settings_, "mta_group", array()) != "mailpoet")) {
            echo "style=\"display:none;\"";
        }
        // line 18
        echo "        >";
        // line 19
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Sign-up confirmation is mandatory when using the MailPoet Sending Service.");
        // line 20
        echo "</p>
        <p
          id=\"mailpoet_signup_confirmation_input\"
          ";
        // line 23
        if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
        if (($this->getAttribute($_settings_, "mta_group", array()) == "mailpoet")) {
            echo "style=\"display:none;\"";
        }
        // line 24
        echo "        >
          <label>
            <input
              type=\"radio\"
              class=\"mailpoet_signup_confirmation\"
              name=\"signup_confirmation[enabled]\"
              value=\"1\"
              ";
        // line 31
        if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
        if ($this->getAttribute($this->getAttribute($_settings_, "signup_confirmation", array()), "enabled", array())) {
            // line 32
            echo "                checked=\"checked\"
              ";
        }
        // line 34
        echo "            />";
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Yes");
        echo "
          </label>
          &nbsp;
          <label>
            <input
              type=\"radio\"
              class=\"mailpoet_signup_confirmation\"
              name=\"signup_confirmation[enabled]\"
              value=\"\"
              ";
        // line 43
        if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
        if ( !$this->getAttribute($this->getAttribute($_settings_, "signup_confirmation", array()), "enabled", array())) {
            // line 44
            echo "                checked=\"checked\"
              ";
        }
        // line 46
        echo "            />";
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("No");
        echo "
          </label>
        </p>
      </td>
    </tr>
  </tbody>
</table>
<table id=\"mailpoet_signup_options\" class=\"form-table\">
  <tbody>
    <!-- sign-up confirmation: from name & email -->
    <tr>
      <th scope=\"row\">
        <label for=\"settings[signup_confirmation_from_name]\">
          ";
        // line 59
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("From");
        echo "
        </label>
      </th>
      <td>
        <p>
          <input
            type=\"text\"
            id=\"settings[signup_confirmation_from_name]\"
            name=\"signup_confirmation[from][name]\"
            value=\"";
        // line 68
        if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_settings_, "signup_confirmation", array()), "from", array()), "name", array()), "html", null, true);
        echo "\"
            placeholder=\"";
        // line 69
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Your name");
        echo "\"
          />
          <input type=\"email\"
            id=\"settings[signup_confirmation_from_email]\"
            name=\"signup_confirmation[from][address]\"
            value=\"";
        // line 74
        if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_settings_, "signup_confirmation", array()), "from", array()), "address", array()), "html", null, true);
        echo "\"
            placeholder=\"confirmation@mydomain.com\"
            size=\"28\"
          />
        </p>
      </td>
    </tr>
    <!-- sign-up confirmation: reply_to name & email -->
    <tr>
      <th scope=\"row\">
        <label for=\"settings[signup_confirmation_reply_name]\">
          ";
        // line 85
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Reply-to");
        echo "
        </label>
      </th>
      <td>
        <p>
          <input
            type=\"text\"
            id=\"settings[signup_confirmation_reply_name]\"
            name=\"signup_confirmation[reply_to][name]\"
            value=\"";
        // line 94
        if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_settings_, "signup_confirmation", array()), "reply_to", array()), "name", array()), "html", null, true);
        echo "\"
            placeholder=\"";
        // line 95
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Your name");
        echo "\"
            />
          <input type=\"email\"
            id=\"settings[signup_confirmation_reply_email]\"
            name=\"signup_confirmation[reply_to][address]\"
            value=\"";
        // line 100
        if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_settings_, "signup_confirmation", array()), "reply_to", array()), "address", array()), "html", null, true);
        echo "\"
            placeholder=\"confirmation@mydomain.com\"
            size=\"28\"
            />
        </p>
      </td>
    </tr>
    <!-- confirmation email: title -->
    <tr>
      <th scope=\"row\">
        <label for=\"settings[signup_confirmation_email_subject]\">
        ";
        // line 111
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Email subject");
        echo "
        </label>
      </th>
      <td>
        <input
          size=\"52\"
          type=\"text\"
          id=\"settings[signup_confirmation_email_subject]\"
          name=\"signup_confirmation[subject]\"
          ";
        // line 120
        if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
        if ($this->getAttribute($this->getAttribute($_settings_, "signup_confirmation", array()), "subject", array())) {
            // line 121
            echo "            value=\"";
            if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_settings_, "signup_confirmation", array()), "subject", array()), "html", null, true);
            echo "\"
          ";
        }
        // line 123
        echo "        />
      </td>
    </tr>
    <!-- confirmation email: body -->
    <tr>
      <th scope=\"row\">
        <label for=\"settings[signup_confirmation_email_body]\">
          ";
        // line 130
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Email content");
        echo "
        </label>
        <p class=\"description\">
          ";
        // line 133
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Don't forget to include:<br /><br />[activation_link]Confirm your subscription.[/activation_link]<br /><br />Optional: [lists_to_confirm].");
        echo "
        </p>
      </th>
      <td>
        <textarea
          cols=\"50\"
          rows=\"15\"
          id=\"settings[signup_confirmation_email_body]\"
          name=\"signup_confirmation[body]\"
        >";
        // line 142
        if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
        if ($this->getAttribute($this->getAttribute($_settings_, "signup_confirmation", array()), "body", array())) {
            // line 143
            if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_settings_, "signup_confirmation", array()), "body", array()), "html", null, true);
        }
        // line 144
        echo "</textarea>
      </td>
    </tr>
    <!-- sign-up confirmation: confirmation page -->
    <tr>
      <th scope=\"row\">
        <label>
          ";
        // line 151
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Confirmation page");
        echo "
        </label>
        <p class=\"description\">
          ";
        // line 154
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("When subscribers click on the activation link, they will be redirected to this page.");
        echo "
        </p>
      </th>
      <td>
        <p>
          <select
            class=\"mailpoet_page_selection\"
            name=\"subscription[pages][confirmation]\"
          >
            ";
        // line 163
        if (isset($context["pages"])) { $_pages_ = $context["pages"]; } else { $_pages_ = null; }
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($_pages_);
        foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
            // line 164
            echo "              <option
                value=\"";
            // line 165
            if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_page_, "id", array()), "html", null, true);
            echo "\"
                data-preview-url=\"";
            // line 166
            if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
            echo $this->getAttribute($this->getAttribute($_page_, "url", array()), "confirm", array());
            echo "\"
                ";
            // line 167
            if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
            if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
            if (($this->getAttribute($_page_, "id", array()) == $this->getAttribute($this->getAttribute($this->getAttribute($_settings_, "subscription", array()), "pages", array()), "confirmation", array()))) {
                // line 168
                echo "                  selected=\"selected\"
                ";
            }
            // line 170
            echo "              >";
            if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_page_, "title", array()), "html", null, true);
            echo "</option>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 172
        echo "          </select>
          <a
            class=\"mailpoet_page_preview\"
            href=\"javascript:;\"
            title=\"";
        // line 176
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Preview page");
        echo "\"
          >";
        // line 177
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Preview");
        echo "</a>
        </p>
      </td>
    </tr>
  </tbody>
</table>

<script type=\"text/javascript\">
  jQuery(function(\$) {
    // om dom loaded
    \$(function() {
      // double optin toggling
      toggleSignupOptions();

      \$('.mailpoet_signup_confirmation').on('click', function() {
        var result = false;

        if(~~(\$(this).val()) === 1) {
          result = confirm(\"";
        // line 195
        echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->env->getExtension('MailPoet\Twig\I18n')->translate("Subscribers will need to activate their subscription via email in order to receive your newsletters. This is highly recommended!"), "js"), "html", null, true);
        echo "\");
        } else {
          result = confirm(\"";
        // line 197
        echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->env->getExtension('MailPoet\Twig\I18n')->translate("New subscribers will be automatically confirmed, without having to confirm their subscription. This is not recommended!"), "js"), "html", null, true);
        echo "\");
        }
        // if the user confirmed changing the sign-up confirmation (yes/no)
        if(result === true) {
          // toggle signup options depending on the currently selected value
          toggleSignupOptions();
        }
        return result;
      });

      function toggleSignupOptions() {
        var is_enabled =
          (~~(\$('.mailpoet_signup_confirmation:checked').val()) === 1);
        \$('#mailpoet_signup_options')[(is_enabled) ? 'show' : 'hide']();
      }
    });
  });
</script>
";
    }

    public function getTemplateName()
    {
        return "settings/signup.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  353 => 197,  348 => 195,  327 => 177,  323 => 176,  317 => 172,  307 => 170,  303 => 168,  299 => 167,  294 => 166,  289 => 165,  286 => 164,  281 => 163,  269 => 154,  263 => 151,  254 => 144,  250 => 143,  247 => 142,  235 => 133,  229 => 130,  220 => 123,  213 => 121,  210 => 120,  198 => 111,  183 => 100,  175 => 95,  170 => 94,  158 => 85,  143 => 74,  135 => 69,  130 => 68,  118 => 59,  101 => 46,  97 => 44,  94 => 43,  81 => 34,  77 => 32,  74 => 31,  65 => 24,  60 => 23,  55 => 20,  53 => 19,  51 => 18,  46 => 17,  37 => 11,  33 => 10,  27 => 7,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "settings/signup.html", "/home/yibraenvolgroupe/public_html/wp-content/plugins/mailpoet/views/settings/signup.html");
    }
}
