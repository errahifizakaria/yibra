<?php
/*
 * Plugin Name: Companion Sitemap Generator
 * Plugin URI: http://codeermeneer.nl/portfolio/companion-sitemap-generator/
 * Description: Easy to use XML & HTML sitemap generator and robots editor.
 * Version: 3.1.3
 * Author: Papin Schipper
 * Author URI: http://codeermeneer.nl
 * Contributors: papin
 * License: GPLv2 or later
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: companion-sitemap-generator
 * Domain Path: /languages/
*/

// Disable direct access
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Load translations
function csg_load_translations() {
	load_plugin_textdomain( 'companion-sitemap-generator', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' ); 
}
add_action( 'init', 'csg_load_translations' );

// Install db
function csg_install() {
	csg_database_creation(); // Db handle
	if (! wp_next_scheduled ( 'csg_create_sitemap' )) wp_schedule_event( time(), 'hourly', 'csg_create_sitemap '); //Set schedule

	// Get files needed for this plugin to work
	$csg_website_root 	= get_home_path();
	$csg_sitemap_file 	= $csg_website_root.'/sitemap.xml';
	$csg_robots_file 	= $csg_website_root.'/robots.txt';

	// Create sitemap file (if it doesn't exist)
	if ( !file_exists( $csg_sitemap_file ) ) $csg_myfile = fopen( $csg_sitemap_file, "w" );

	// Create robots file (if it doesn't exist)
	if ( !file_exists( $csg_robots_file ) ) $csg_robots_myfile = fopen( $csg_robots_file, "w" );
}
add_action('csg_create_sitemap', 'csg_sitemap');

function csg_database_creation() {

	global $wpdb;
	global $csg_db_version;

	$csg_db_version = '3.1.0';

	// Create db table
	$table_name = $wpdb->prefix . "sitemap"; 

	$sql = "CREATE TABLE $table_name (
		id INT(9) NOT NULL AUTO_INCREMENT,
		name VARCHAR(255) NOT NULL,
		onoroff VARCHAR(255) NOT NULL,
		UNIQUE KEY id (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	// Database version
	add_option( "csg_db_version", "$csg_db_version" );

	// Insert data
	csg_install_data();

	// Updating..
	$installed_ver = get_option( "csg_db_version" );
	if ( $installed_ver != $csg_db_version ) update_option( "csg_db_version", $csg_db_version );

}

// Check if database table exists before creating
function csg_check_if_exists( $whattocheck ) {

	global $wpdb;
	$table_name = $wpdb->prefix . "sitemap"; 

	$rows 	= $wpdb->get_col( "SELECT COUNT(*) as num_rows FROM $table_name WHERE name = '$whattocheck'" );
	$check 	= $rows[0];

	if( $check > 0) {
		return true;
	} else {
		return false;
	}

}

// Insert Data
function csg_install_data() {

	global $wpdb;
	$table_name = $wpdb->prefix . "sitemap"; 

	if( !csg_check_if_exists( 'exclude' ) ) $wpdb->insert( $table_name, array( 'name' => 'exclude', 'onoroff' => '' ) );
	if( !csg_check_if_exists( 'posttypes' ) ) $wpdb->insert( $table_name, array( 'name' => 'posttypes', 'onoroff' => '' ) );

}
register_activation_hook( __FILE__, 'csg_install' );

// Clear everything
function csg_remove() {
	global $wpdb;
	$table_name = $wpdb->prefix . "sitemap"; 
	$wpdb->query( "DROP TABLE IF EXISTS $table_name" );
	wp_clear_scheduled_hook('csg_create_sitemap');
}
register_deactivation_hook(  __FILE__, 'csg_remove' );

// Update
function csg_update_db_check() {
    global $csg_db_version;
    if ( get_site_option( 'csg_db_version' ) != $csg_db_version ) {
        csg_database_creation();
    }
}
add_action( 'plugins_loaded', 'csg_update_db_check' );

// Creates the sitemap
function csg_sitemap() {

	$csg_sitemap_file = get_home_path().'/sitemap.xml';

	if ( file_exists( $csg_sitemap_file ) ) {

		if ( is_writable( $csg_sitemap_file ) ) {

				// First clear + write sitemap
				file_put_contents( $csg_sitemap_file, '' );
				file_put_contents( $csg_sitemap_file, csg_sitemap_content() );

				// Succes
				succesMSG( '<b>'.__( 'Your sitemap has been updated', 'companion-sitemap-generator' ).'</b> '.__( 'Check it out', 'companion-sitemap-generator' ).': <a href="'. get_site_url() .'/sitemap.xml" target="_blank">'. get_site_url() .'/sitemap.xml</a>' );

		} else {

			// Error when sitemap.xml is not writable
		    errorMSG( __( 'Your sitemap file is not writable', 'companion-sitemap-generator').'</p></div>' );

			$subject 		= __('Sitemap Error', 'companion-sitemap-generator');
			$message 		= __( 'Something went wrong while updating your sitemap: ', 'companion-sitemap-generator' );
			$message 		.= __( 'Your sitemap file is not writable.', 'companion-sitemap-generator' );

			wp_mail( get_option('admin_email') , $subject, $message, $headers );

		}

	} else {

		// Error if sitemap.xml doesn't exist
		errorMSG('<p>'.__( 'We weren\'t able to locate a sitemap file in your website\'s root folder. ', 'companion-sitemap-generator' ).'</p>');

		$subject 		= __('Sitemap Error', 'companion-sitemap-generator');
		$message 		= __( 'Something went wrong while updating your sitemap: ', 'companion-sitemap-generator' );
		$message 		.= __( 'We weren\'t able to locate a sitemap file in your website\'s root folder.', 'companion-sitemap-generator' );

		wp_mail( get_option('admin_email') , $subject, $message, $headers );

	}

}

// This function writes to the sitemap file
function csg_sitemap_content() {

	// Basic XML output
	$csg_sitemap_content = '<?xml version="1.0" encoding="UTF-8"?>
	<urlset xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';


	// Pages
	if( !in_array( 'page', csg_exclude_posttypes() ) ) {

		$csg_sitemap_args = array( 
			'order' => 'asc', 
			'post_type' => 'page', 
			'posts_per_page' => '-1', 
			'post_status' => 'publish', 
			'post__not_in' => csg_exclude()
		);

		query_posts( $csg_sitemap_args );

		if( have_posts() ) {

			while( have_posts() ) {

				the_post();

				$csg_sitemap_content .= '
				<url>
					<loc>'. get_the_permalink() .'</loc>
				</url>
				';

			}

		}

		wp_reset_query();

	}

	// Posts
	if( !in_array( 'post', csg_exclude_posttypes() ) ) {

		$csg_sitemap_args = array( 
			'order' => 'asc', 
			'post_type' => 'post', 
			'posts_per_page' => '-1', 
			'post_status' => 'publish', 
			'post__not_in' => csg_exclude()
		);

		query_posts( $csg_sitemap_args );

		if( have_posts() ) {

			while( have_posts() ) {

				the_post();

				$csg_sitemap_content .= '
				<url>
					<loc>'. get_the_permalink() .'</loc>
				</url>
				';

			}

		}

		wp_reset_query();

	}


	// All the other post types
	$args2 = array(
	   'public'   => true,
	   '_builtin' => false
	);

	$output 		= 'names';
	$operator 		= 'and';

	$post_types2 	= get_post_types( $args2, $output, $operator ); 

	foreach ( $post_types2  as $post_type ) {

		if( !in_array( $post_type, csg_exclude_posttypes() ) ) {

			$csg_sitemap_args = array( 
				'order' => 'asc', 
				'post_type' => $post_type, 
				'posts_per_page' => '-1', 
				'post_status' => 'publish', 
				'post__not_in' => csg_exclude()
			);

			query_posts( $csg_sitemap_args );

			if( have_posts() ) {

				while( have_posts() ) {

					the_post();

					$csg_sitemap_content .= '
					<url>
						<loc>'. get_the_permalink() .'</loc>
					</url>
					';

				}

			}

			wp_reset_query();

		}

	}

	$csg_sitemap_content .= '</urlset>';

	// Return sitemap-string but first filter any text containing illegal named entities
	return ent2ncr( $csg_sitemap_content );

}

// Get all post types without filtered once
function csg_get_filtered_post_types() {

	$pttArray = csg_get_post_types();

	foreach ( $pttArray as $postType ) {

		if( in_array( $postType, csg_exclude_posttypes() ) ) {

			if( ( $key = array_search( $postType, $pttArray ) ) !== false ) {
			    unset( $pttArray[ $key ] );
			}

		}

	}

}

// Get all post types
function csg_get_post_types() {

	// Get all exisiting default post types
	$args = array(
	   'public'   => true,
	   '_builtin' => true
	);

	// Get all exisiting custom post types
	$args2 = array(
	   'public'   => true,
	   '_builtin' => false
	);

	$output 		= 'names';
	$operator 		= 'and';

	$post_types 	= get_post_types( $args, $output, $operator ); 
	$post_types2 	= get_post_types( $args2, $output, $operator ); 

	$post_type_array = array();

	foreach ( $post_types  as $post_type ) array_push( $post_type_array , $post_type );
	foreach ( $post_types2  as $post_type ) array_push( $post_type_array , $post_type );

	return $post_type_array;

}

// Custom functions for handling messages
function succesMSG( $content ) {
	echo '<div id="message" class="updated"><p>'.$content.'</p></div>';
}
function errorMSG( $content ) {
	echo '<div id="message" class="error"><p>'.$content.'</p></div>';
}

// Add to menu
function csg_menu_items(){

	add_submenu_page( 'tools.php', __('Sitemap', 'companion-sitemap-generator'), __('Sitemap', 'companion-sitemap-generator'), 'manage_options', 'csg-sitemap', 'csg_dashboard' );
	add_submenu_page( 'tools.php', __('Robots.txt', 'companion-sitemap-generator'), __('Robots.txt', 'companion-sitemap-generator'), 'manage_options', 'csg-robots', 'csg_robots_dashboard' );

}
add_action( 'admin_menu', 'csg_menu_items' );


// Sitemap dashboard
function csg_dashboard() {
	include_once( 'sitemap.php' );
}

// Robots dashboard
function csg_robots_dashboard() {
	include_once( 'robots.php' );
}

// Create widget
include_once( 'widget.php' );

// Add generate sitemap link on plugin page
function csg_settings_link( $links ) { 
	$settings_link = '<a href="tools.php?page=csg-sitemap">'.__('Settings', 'companion-sitemap-generator' ).'</a>'; 
	$settings_link2 = '<a href="https://translate.wordpress.org/projects/wp-plugins/companion-sitemap-generator" target="_blank">'.__('Help us translate', 'companion-sitemap-generator' ).'</a>'; 
	array_unshift( $links, $settings_link ); 
	array_unshift( $links, $settings_link2 ); 
	return $links; 
}
$plugin = plugin_basename(__FILE__); 
add_filter("plugin_action_links_$plugin", 'csg_settings_link' );

// Load admin styles
function load_csg_styles( $hook ) {

        if( $hook != 'tools_page_csg-sitemap' ) return;
        wp_enqueue_style( 'csg_admin_styles', plugins_url('backend/style.css', __FILE__) );

}
add_action( 'admin_enqueue_scripts', 'load_csg_styles' );

// Load admin javascript files
function csg_enqueue_plugin_scripts( $plugin_array ) {
    $plugin_array["csg_mce_button"] =  plugin_dir_url(__FILE__) . "backend/javascript.js";
    return $plugin_array;
}
add_filter( "mce_external_plugins", "csg_enqueue_plugin_scripts" );

// Get PAGES for html sitemap
function html_pages() {

	if( !in_array( 'page', csg_exclude_posttypes() ) ) {

		$csg_sitemap_args = array( 
			'order' => 'asc', 
			'post_type' => 'page', 
			'posts_per_page' => '-1', 
			'post_status' => 'publish', 
			'post__not_in' => csg_exclude(),
			'post_parent' => 0
		);

		query_posts( $csg_sitemap_args );

		if( have_posts() ) {

			$pages = '<div class="html-sitemap-column"><h2>'.__( "Pages", "companion-sitemap-generator" ).'</h2>
			<ul class="html-sitemap-list">';

			while ( have_posts() ) {
				the_post(); 
				$pages .= '<li class="html-sitemap-list-item"><a href="'. get_the_permalink() .'" title="'. get_the_title() .'">'. get_the_title() .'</a></li>';
				$args = array(
					'post_parent' => get_the_ID(),
					'post_type'   => 'page', 
					'posts_per_page' => '-1', 
					'post_status' => 'publish',
					'post__not_in' => csg_exclude(),
				);
				$children = get_children( $args );

				if( !empty( $children ) ) {
					$pages .= '<ul class="html-sitemap-sub-list">';
				}

				foreach ( $children as $child ) {
					$pages .= '<li class="html-sitemap-list-item"><a href="'. get_the_permalink( $child->ID ) .'" title="'. get_the_title( $child->ID ) .'">'. get_the_title( $child->ID ) .'</a></li>';
				}

				if( !empty( $children ) ) {
					$pages .= '</ul>';
				}

			}

			$pages .= '</ul></div>';

		}

		wp_reset_query();

		return $pages;

	}

}

// Get POSTS for html sitemap
function html_posts() {

	if( !in_array( 'post', csg_exclude_posttypes() ) ) {

		$csg_sitemap_args = array( 
			'order' => 'asc', 
			'post_type' => 'post', 
			'posts_per_page' => '-1', 
			'post_status' => 'publish', 
			'post__not_in' => csg_exclude()
		);

		query_posts( $csg_sitemap_args );

		if( have_posts() ) {

			$posts = '<div class="html-sitemap-column"><h2>'.__( "Posts", "companion-sitemap-generator" ).'</h2>
			<ul class="html-sitemap-list">';

			while ( have_posts() ) {
				the_post(); 
				$posts .= '<li class="html-sitemap-list-item"><a href="'. get_the_permalink() .'" title="'. get_the_title() .'">'. get_the_title() .'</a></li>';
			}

			$posts .= '</ul></div>';

		}

		wp_reset_query();

		return $posts;

	}

}

// Get all other post types for html sitemap
function html_posttypes() {

	$args2 = array(
	   'public'   => true,
	   '_builtin' => false
	);

	$output 		= 'names';
	$operator 		= 'and';

	$post_types2 	= get_post_types( $args2, $output, $operator ); 

	foreach ( $post_types2  as $post_type ) {

		if( !in_array( $post_type, csg_exclude_posttypes() ) ) {

			$csg_sitemap_args = array( 
				'order' => 'asc', 
				'post_type' => $post_type, 
				'posts_per_page' => '-1', 
				'post_status' => 'publish', 
				'post__not_in' => csg_exclude()
			);

			query_posts( $csg_sitemap_args );

			if( have_posts() ) {

				$posts = '<div class="html-sitemap-column"><h2>'.__( "Post type", "companion-sitemap-generator" ).': '.$post_type.'</h2>
				<ul class="html-sitemap-list">';

				while ( have_posts() ) {
					the_post(); 
					$posts .= '<li class="html-sitemap-list-item"><a href="'. get_the_permalink() .'" title="'. get_the_title() .'">'. get_the_title() .'</a></li>';
				}

				$posts .= '</ul></div>';

			}

			wp_reset_query();

		}

	}

	return $posts;

}

// Create shortcode
function htmlsitemap() {

	$csg_sitemap_content = html_pages();
	$csg_sitemap_content .= html_posts();
	$csg_sitemap_content .= html_posttypes();

	return $csg_sitemap_content;
}
add_shortcode( 'html-sitemap' , 'htmlsitemap' );


// Exclude these posts from the sitemap
function csg_exclude() {

	global $wpdb;
	$table_name 	= $wpdb->prefix . "sitemap"; 
	$config 		= $wpdb->get_results( "SELECT * FROM $table_name WHERE name = 'exclude'");

	$list 			= $config[0]->onoroff;
	$list 			= explode( ", ", $list );
	$returnList 	= array();

	foreach ( $list as $key ) array_push( $returnList, $key );
	
	return $returnList;

}

// Exclude these posttypes from the sitemap
function csg_exclude_posttypes() {

	global $wpdb;
	$table_name 	= $wpdb->prefix . "sitemap"; 
	$config 		= $wpdb->get_results( "SELECT * FROM $table_name WHERE name = 'posttypes'");

	$list 			= $config[0]->onoroff;
	$list 			= explode( ", ", $list );
	$returnList 	= array();

	foreach ( $list as $key ) array_push( $returnList, $key );
	
	return $returnList;

}

// Get active tab
function csg_active_tab( $page ) {

	if( !isset( $_GET['tabbed'] ) ) {
		$cur_page = '';
	} else {
		$cur_page = $_GET['tabbed'];
	}

	if( $page == $cur_page ) {
		echo 'nav-tab-active';
	}

}

function change_footer_admin ( ) {  
	echo '<a href="https://wordpress.org/plugins/companion-auto-update" target="_blank">';
	_e( 'Keep your WordPress site secure and up-to-date with Companion Auto Update', 'companion-sitemap-generator' );
	echo '</a>.'; 
}  

if( $_GET['page'] == 'csg-sitemap' OR $_GET['page'] == 'csg-robots' ) {
	add_filter('admin_footer_text', 'change_footer_admin');
} 

// Add button to editor
function csg_sitemap_config_btn() {
    echo '<a id="csg-shortcode-insert" class="button"><span class="dashicons dashicons-networking" style="position: relative; bottom: -2px;"></span> '.__('HTML sitemap', 'companion-sitemap-generator').'</a>';
}
add_action('media_buttons', 'csg_sitemap_config_btn', 20);

?>