<?php

$post_types 	= get_post_types( array( 'public' => true, '_builtin' => true ), 'names', 'and' ); 
$post_types2 	= get_post_types( array( 'public' => true, '_builtin' => false ), 'names', 'and' ); 

if( isset( $_POST['submit'] ) ) {

	global $wpdb;
	$table_name = $wpdb->prefix . "sitemap";

	foreach ( $post_types  as $post_type ) {
		if( $post_type != 'attachment' ) {
			if( $_POST[$post_type] == '' ) {
				$exclPostTypes .= $post_type.', ';
			}
		}
	}
	foreach ( $post_types2  as $post_type ) {
		if( $_POST[$post_type] == '' ) {
			$exclPostTypes .= $post_type.', ';
		}
	}

	$wpdb->query( " UPDATE $table_name SET onoroff = '$exclPostTypes' WHERE name = 'posttypes' " );
	echo '<div id="message" class="updated"><p><b>'.__('Succes', 'companion-sitemap-generator').' &ndash;</b> '.sprintf( esc_html__( 'Unchecked post types will no longer be added to your sitemap.', 'companion-sitemap-generator' ), $excludeCounter ).'.</p></div>';
}

?>

<p><?php _e('Here you can select postttypes that you do not want to include in your sitemap.', 'companion-sitemap-generator'); ?> <br />

<form method="POST">

	<table class="form-table">

	<?php foreach ( $post_types  as $post_type ) {
		if( $post_type != 'attachment' ) { ?>
			<tr>
				<th scope="row"><?php echo ucfirst( $post_type ); ?></th>
				<td>
					<fieldset>

						<input id='<?php echo $post_type; ?>' name='<?php echo $post_type; ?>' type='checkbox' <?php if( !in_array( $post_type, csg_exclude_posttypes() ) ) { echo "CHECKED"; } ?> > <label for="<?php echo $post_type; ?>"><?php _e('Uncheck to exclude.', 'companion-sitemap-generator'); ?></label>

					</fieldset>
				</td>
			</tr>
		<?php }
	}

	foreach ( $post_types2  as $post_type ) { ?>
		<tr>
			<th scope="row"><?php echo ucfirst( $post_type ); ?></th>
			<td>
				<fieldset>

					<input id='<?php echo $post_type; ?>' name='<?php echo $post_type; ?>' type='checkbox' <?php if( !in_array( $post_type, csg_exclude_posttypes() ) ) { echo "CHECKED"; } ?> > <label for="<?php echo $post_type; ?>"><?php _e('Uncheck to exclude.', 'companion-sitemap-generator'); ?></label>

				</fieldset>
			</td>
		</tr>
	<?php } ?>

	</table>

	<?php submit_button(); ?>

</form>