<div class="wrap">

		<h1 class="wp-heading-inline"><?php _e('Sitemap', 'companion-sitemap-generator'); ?></h1>

 		<a href="https://translate.wordpress.org/projects/wp-plugins/companion-sitemap-generator/" target="_blank" class="page-title-action"><?php _e('Help us translate', 'companion-sitemap-generator'); ?></a>
 		<a href="https://www.paypal.me/dakel/1/" target="_blank" class="page-title-action"><?php _e('Donate to help development', 'companion-sitemap-generator'); ?></a>

		<hr class="wp-header-end">

		<h2 class="nav-tab-wrapper wp-clearfix">
			<a href="tools.php?page=csg-sitemap" class="nav-tab <?php csg_active_tab(''); ?>"><?php _e('Dashboard', 'companion-sitemap-generator'); ?></a>
			<a href="tools.php?page=csg-sitemap&amp;tabbed=posttypes" class="nav-tab <?php csg_active_tab('posttypes'); ?>"><?php _e('Exclude Post types', 'companion-sitemap-generator'); ?></a>
			<a href="tools.php?page=csg-sitemap&amp;tabbed=select" class="nav-tab <?php csg_active_tab('select'); ?>"><?php _e('Exclude Posts', 'companion-sitemap-generator'); ?></a>
		</h2>

	<?php 

	if( $_GET['tabbed'] == 'select' ) {
		
		require( 'select-posts.php' );

	} else if( $_GET['tabbed'] == 'posttypes' ) {
		
		require( 'select-post-types.php' );

	} else { 
		
		if( isset( $_POST['csg_generate'] ) ) csg_sitemap();

		?>

		<table class="form-table">
			<tr>
				<th scope="row"><?php _e('Sitemap link', 'companion-sitemap-generator');?></th>
				<td>
					<fieldset>

						<p><?php echo site_url(); ?>/sitemap.xml</p>
						<p><a href='<?php echo site_url(); ?>/sitemap.xml' class='button' target='_blank'><?php _e('View sitemap', 'companion-sitemap-generator');?></a></p>

					</fieldset>
				</td>
			</tr>

			<tr>
				<th scope="row"><?php _e('Update Sitemap', 'companion-sitemap-generator');?></th>
				<td>
					<fieldset>
						<p><?php _e('We update your sitemap every hour, but in case you\'d like to update it manually you can do that here.', 'companion-sitemap-generator');?>.</p>
						<form method="POST">
							<p><button type="submit" name="csg_generate" class="button button-alt"><?php echo csg_dynamic_button(); ?></button></a></p>
						</form>
					</fieldset>
				</td>
			</tr>

			<tr>
				<th scope="row"><?php _e('HTML Sitemap', 'companion-sitemap-generator');?></th>
				<td>
					<fieldset>

						<p><?php _e('Use this shortcode to display an HTML sitemap:', 'companion-sitemap-generator');?></p>
						<p><code>[html-sitemap]</code></p>

					</fieldset>
				</td>
			</tr>

			<tr>
				<th scope="row"><?php _e('Sitemap & Robots', 'companion-sitemap-generator');?></th>
				<td>
					<fieldset>

						<p><?php _e('Use this line if you\'d like to add the sitemap link to the robots file (good for SEO):', 'companion-sitemap-generator');?></p>
						<p><code>Sitemap: <?php echo site_url(); ?>/sitemap.xml</code></p>

					</fieldset>
				</td>
			</tr>
			</tr>
		</table>

	<?php } ?>

</div>