<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', '/home/yibraenvolgroupe/public_html/wp-content/plugins/wp-super-cache/' );
define('DB_NAME', 'yibraenv_db');

/** MySQL database username */
define('DB_USER', 'yibraenv_admin');

/** MySQL database password */
define('DB_PASSWORD', '#U#%)F5=JQYK');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'tY7Tk671G9Lp0YPDAD})GV(`zWV|xma_9VQy#GA(SG{^_49yQlX(-~wcY$p<}b}K');
define('SECURE_AUTH_KEY',  'sE8+LG!]xGcd@J{TwVjA:BJs:N-rv?c6!~YpP}]NwU`4skCJqgiyY(Z@1;sIc>y|');
define('LOGGED_IN_KEY',    'qKCT~k2 @<Euj=@,~VYR{d,{Pd)F;[?gNY07kMHE{1WyCpgVoP~TXL.BVhoX%_YR');
define('NONCE_KEY',        'vpb3k|y{NNo{r`lV(gcH|/,cFcN),gR#H|G+YJ`1$|rN=7K20Q.){=joDPun3-TL');
define('AUTH_SALT',        'p*v0E~yc56uH^{bE^dZUj5>yQ-RR=M@.!,8p_T+9&T|KmKYQo*7 ;DoVLnt!R9jk');
define('SECURE_AUTH_SALT', 'B:.tUn.q%|r3$bk<gUeFw*N/a@j|4QTZCYG+a`!Qt$Wu1chO+So5%glmlI@6sxuH');
define('LOGGED_IN_SALT',   'k~vlkjL]w8-4aROW.zId$:7t9L/;_Lhn?KX4EBS&[R=ap>KPI&QM^tlmoy+Dc$Kq');
define('NONCE_SALT',       '(E@xt}Gim7??mI3 7N7_;3?YV1qM*YdhHr~z5nCxNeO6%*EWT~O3862EJUkW[a7L');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
